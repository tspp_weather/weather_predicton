import QtQuick 2.10
import WeatherPrediction 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0

Item {
    id: mainwindow
    width: Constants.width
    height: Constants.height


    Column {
        id: toolbar
        x: 20
        y: 60
        width: 60
        height: 700
        z: 1
        spacing: 20

        Rectangle {
            id: home_button
            width: 60
            height: 60
            color: "#00000000"
            border.width: 0

            Image {
                id: home_img
                width: 60
                height: 60
                source: "res/home2.png"
            }

            Rectangle {
                id: home_text
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                opacity: 1
                z: -1
            }

            Text {
                id: element
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("HOME")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
            }

            RoundButton {
                id: roundButton2
                x: 0
                y: 0
                width: 60
                height: 60
                text: ""
            }

        }

        Rectangle {
            id: advice_button
            y: 100
            width: 60
            height: 60
            color: "#00000000"
            border.width: 0
            Image {
                id: advice_img
                width: 60
                height: 60
                source: "res/category2.png"
            }

            Rectangle {
                id: advice_text
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                z: -1
            }

            Text {
                id: categories
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("ADVICES")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            RoundButton {
                id: roundButton1
                width: 60
                height: 60
                text: ""
            }
        }

        Rectangle {
            id: exit_button
            y: 0
            width: 60
            height: 60
            color: "#00000000"

            Image {
                id: cross_img
                width: 60
                height: 60
                source: "res/cross2.png"
            }

            Rectangle {
                id: exit_text2
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                z: -1
            }

            Text {
                id: exit
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("EXIT")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            RoundButton {
                id: roundButton
                x: 0
                y: 0
                width: 60
                height: 60
                text: ""
            }


        }




    }

    Image {
        id: background
        x: 0
        y: 0
        width: 1286
        height: 720
        z: -1
        source: "res/wallpapersden.com_anime-girl-and-colorful-sky_2560x1440.jpg"
    }

    Rectangle {
        id: weather_widget
        x: 260
        y: 0
        width: 400
        height: 500
        color: "#00000000"
        radius: 1
        border.color: "#00000000"
        border.width: 5

        Image {
            id: weather_ico
            x: 125
            y: 40
            width: 150
            height: 150
            source: "res/weather_icons/1x/Asset 111.png"
        }

        Text {
            id: temp
            x: 100
            y: 195
            width: 200
            height: 80
            color: "#78bdd8"
            text: qsTr("20°С")
            opacity: 1
            font.family: "Arial"
            lineHeight: 0.9
            wrapMode: Text.WrapAnywhere
            renderType: Text.NativeRendering
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideNone
            styleColor: "#00000000"
            font.pixelSize: 50
        }

        Rectangle {
            id: temp_background
            x: 100
            y: 195
            width: 200
            height: 80
            color: "#000731"
            radius: 40
            opacity: 0.5
            z: -1
        }

        Column {
            id: params_back
            y: 280
            width: 400
            height: 215
            spacing: 10

            Rectangle {
                id: pressure_back
                width: 400
                height: 40
                color: "#000731"
                radius: 20
                opacity: 0.5
            }

            Rectangle {
                id: humidity_back
                width: 400
                height: 40
                color: "#000731"
                radius: 20
                opacity: 0.5
            }

            Rectangle {
                id: wind_speed_back
                width: 400
                height: 40
                color: "#000731"
                radius: 20
                opacity: 0.5
            }
        }

        ColumnLayout {
            id: params_names
            x: 20
            y: 280
            width: 170
            height: 140

            Text {
                id: pressure
                y: 0
                width: 134
                color: "#78bdd8"
                text: "Humidity: "
                Layout.preferredHeight: 40
                Layout.preferredWidth: 134
                font.pixelSize: 30
            }

            Text {
                id: humidity
                width: 130
                color: "#78bdd8"
                text: "Pressure: "
                Layout.preferredHeight: 40
                Layout.preferredWidth: 129
                font.pixelSize: 30
            }

            Text {
                id: wind_speed
                y: 100
                color: "#78bdd8"
                text: "Wind speed: "
                Layout.preferredHeight: 40
                Layout.preferredWidth: 170
                font.pixelSize: 30
            }
        }

        Text {
            id: humidity_stat
            x: 155
            y: 280
            width: 233
            height: 40
            color: "#78bdd8"
            text: "20%"
            Layout.preferredWidth: 134
            Layout.preferredHeight: 40
            font.pixelSize: 30
        }

        Text {
            id: pressure_stat
            x: 155
            y: 330
            width: 235
            height: 40
            color: "#78bdd8"
            text: "300 Pa"
            Layout.preferredWidth: 134
            Layout.preferredHeight: 40
            font.pixelSize: 30
        }

        Text {
            id: wind_peed_stat
            x: 195
            y: 380
            width: 195
            height: 40
            color: "#78bdd8"
            text: "40 km/h"
            Layout.preferredWidth: 134
            Layout.preferredHeight: 40
            font.pixelSize: 30
        }



    }


    SwipeView {
        id: short_recom
        x: 700
        y: 60
        width: 500
        height: 612
        currentIndex: 0
        focusPolicy: Qt.TabFocus
        Item {
            id: advices_sum
            width: 500
            height: 610
            visible: true
            Rectangle {
                id: advices_name_back
                x: 50
                width: 300
                height: 50
                color: "#000731"
                radius: 25
                opacity: 0.5
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Rectangle {
                id: advicee
                x: 20
                y: 60
                width: 500
                height: 540
                color: "#000731"
                radius: 25
                opacity: 0.5
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ColumnLayout {
                id: advice_pack
                x: 50
                y: 80
                width: 400
                height: 500
                visible: true
                Text {
                    id: scenario1
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- You should go for a walk!")
                    visible: true
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario2
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- No badminton!")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario3
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: "-- Queen, today!!"
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario4
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- Maybe, some picnic?")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }
                spacing: 10
            }

            Text {
                id: scenarioo
                x: 50
                y: 0
                width: 400
                height: 50
                color: "#78bdd8"
                text: qsTr("TOP ADVICES")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 30
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Item {
            x: 0
            y: 0
            width: 540
            height: 610
            visible: true
            Rectangle {
                id: scenarios_name_back2
                width: 300
                height: 50
                color: "#000731"
                radius: 25
                opacity: 0.5
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Rectangle {
                id: scenario20
                x: 20
                y: 60
                width: 500
                height: 540
                color: "#000731"
                radius: 25
                opacity: 0.5
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Text {
                id: scenarioo2
                x: 100
                y: 0
                width: 300
                height: 50
                color: "#78bdd8"
                text: qsTr("ADVICES")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 30
                horizontalAlignment: Text.AlignHCenter
            }

            ColumnLayout {
                id: advice_pack1
                x: 50
                y: 80
                width: 400
                height: 500
                visible: true
                Text {
                    id: scenario5
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- You should go for a walk!")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario6
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- Play outside!")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario7
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: "-- Maybe, picnic?"
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: scenario8
                    x: 0
                    y: 0
                    width: 400
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("-- Visit Queen today!")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                }
                spacing: 10
            }
        }
        spacing: 200
        hoverEnabled: true
    }

    Connections {
        target: roundButton1
        onClicked: {
        var component = Qt.createComponent("Advice_window.qml")
        var window    = component.createObject(mainwindow)
            window.show
        }
    }

    Connections {
        target: roundButton2
        onClicked: {
            var component = Qt.createComponent("MAin_window.qml")
            var window    = component.createObject(mainwindow)
            window.show
        }
    }

    Connections {
        target: roundButton
        onClicked: {
            Qt.quit()
        }
    }


}




















