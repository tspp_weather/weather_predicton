import QtQuick 2.10
import WeatherPrediction 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Studio.Components 1.0

Rectangle {
    id: mainwindow
    width: Constants.width
    height: Constants.height

    Image {
        id: image
        x: 0
        y: 0
        width: 1280
        height: 720
        source: "res/wallpapersden.com_anime-girl-and-colorful-sky_2560x1440.jpg"

        Rectangle {
            id: mail_background
            x: 483
            y: 275
            width: 315
            height: 50
            color: "#ffffff"
            radius: 8

            TextInput {
                id: mail_input
                x: 5
                y: 8
                width: 300
                height: 22
                color: "#9393ac"
                echoMode: TextInput.Normal
                text: qsTr("vlad.spitkovsky@gmail.com")
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                validator: RegExpValidator { regExp: /vlad.spitkovsky@gmail.com/ }
            }
        }

        Rectangle {
            id: submit_background
            x: 483
            y: 515
            width: 315
            height: 50
            color: "#2097f1"
            radius: 8
            opacity: 1
            visible: true

            Button {
                id: button
                x: 0
                y: 0
                width: 315
                height: 50
                text: qsTr("Submit")
                hoverEnabled: true
                checkable: true
                padding: 9
                font.family: "Arial"
                font.pointSize: 20
                focusPolicy: Qt.ClickFocus
                display: AbstractButton.TextBesideIcon
                enabled: mail_input.acceptableInput && pass_input.acceptableInput
            }
        }

        Rectangle {
            id: pass_back
            x: 483
            y: 335
            width: 315
            height: 50
            color: "#ffffff"
            radius: 8
            TextInput {
                id: pass_input
                x: 5
                y: 8
                width: 300
                height: 25
                color: "#9393ac"
                echoMode: TextInput.PasswordEchoOnEdit
                text: qsTr("Password")
                passwordCharacter: qsTr("●")
                validator: RegExpValidator {
                    regExp: /Password/
                }
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                horizontalAlignment: Text.AlignHCenter

            }
        }

        Rectangle {
            id: pass_back1
            x: 483
            y: 395
            width: 315
            height: 50
            color: "#ffffff"
            radius: 8
            TextInput {
                id: pass_input1
                x: 5
                y: 8
                width: 300
                height: 25
                color: "#9393ac"
                text: qsTr("Password")
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                passwordCharacter: qsTr("●")
                anchors.verticalCenter: parent.verticalCenter
                validator: RegExpValidator {
                    regExp: /Password/
                }
                echoMode: TextInput.PasswordEchoOnEdit
            }
        }

        Rectangle {
            id: city
            x: 483
            y: 455
            width: 315
            height: 50
            color: "#ffffff"
            radius: 8
            TextInput {
                id: city_input
                x: 5
                y: 8
                width: 300
                height: 22
                color: "#9393ac"
                text: qsTr("Kyiv")
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                anchors.verticalCenter: parent.verticalCenter
                validator: RegExpValidator {
                    regExp: /Kyiv/
                }
                echoMode: TextInput.Normal
            }
        }

        Image {
            id: logo
            x: 191
            y: 111
            width: 900
            height: 100
            source: "res/weather_io.png"
            Rectangle {
                id: rectangle
                x: 715
                y: 80
                width: 20
                height: 20
                color: "#ffffff"
                radius: 10
            }
        }

    }

    Connections {
        target: button
        onClicked: {
            var component = Qt.createComponent("MAin_window.qml")
            var window    = component.createObject(mainwindow)
            window.show
        }
    }

}
