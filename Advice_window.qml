import QtQuick 2.10
import WeatherPrediction 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0

Item {
    id: mainwindow
    width: Constants.width
    height: Constants.height


    Column {
        id: toolbar
        x: 20
        y: 60
        width: 60
        height: 610
        z: 2
        spacing: 20

        Rectangle {
            id: home_button
            width: 60
            height: 60
            color: "#00000000"
            border.width: 0
            Image {
                id: home_img
                width: 60
                height: 60
                source: "res/home2.png"
            }

            Rectangle {
                id: home_text
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                opacity: 1
                z: -1
            }

            Text {
                id: element
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("HOME")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
                horizontalAlignment: Text.AlignHCenter
            }

            RoundButton {
                id: roundButton2
                x: 0
                y: 0
                width: 60
                height: 60
                text: ""
            }
        }

        Rectangle {
            id: advice_button
            y: 100
            width: 60
            height: 60
            color: "#00000000"
            border.width: 0
            Image {
                id: advice_img
                width: 60
                height: 60
                source: "res/category2.png"
            }

            Rectangle {
                id: advice_text
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                z: -1
            }

            Text {
                id: categories
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("ADVICES")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
                horizontalAlignment: Text.AlignHCenter
            }

            RoundButton {
                id: roundButton1
                width: 60
                height: 60
                text: ""
            }
        }

        Rectangle {
            id: exit_button
            y: 0
            width: 60
            height: 60
            color: "#00000000"
            Image {
                id: cross_img
                width: 60
                height: 60
                source: "res/cross2.png"
            }

            Rectangle {
                id: exit_text2
                x: 0
                y: 0
                width: 200
                height: 60
                color: "#000d56"
                radius: 30
                z: -1
            }

            Text {
                id: exit
                x: 56
                y: 0
                width: 120
                height: 60
                color: "#ffffff"
                text: qsTr("EXIT")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
                horizontalAlignment: Text.AlignHCenter
            }

            RoundButton {
                id: roundButton
                x: 0
                y: 0
                width: 60
                height: 60
                text: ""
            }
        }



    }

    Image {
        id: background
        x: 0
        y: 0
        width: 1286
        height: 720
        z: -1
        source: "res/wallpapersden.com_anime-girl-and-colorful-sky_2560x1440.jpg"
    }

    Rectangle {
        id: advice_win
        x: 300
        y: 60
        width: 800
        height: 600
        color: "#00000000"
        visible: true

        Rectangle {
            id: advices_name_back
            x: 0
            width: 300
            height: 50
            color: "#000731"
            radius: 25
            opacity: 0.5
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Rectangle {
            id: advicee
            x: 20
            y: 60
            width: 800
            height: 540
            color: "#000731"
            radius: 25
            opacity: 0.5
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: scenarioo
            x: 50
            y: 0
            width: 400
            height: 50
            color: "#78bdd8"
            text: qsTr("ADVICES")
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
        }

        RowLayout {
            id: advice_toolbar
            x: 580
            y: 0
            width: 210
            height: 50
            spacing: 5

            Rectangle {
                id: trash_bin
                width: 50
                height: 50
                color: "#00000000"

                Image {
                    id: trash_bin_icon
                    x: 0
                    y: 0
                    width: 40
                    height: 40
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    source: "res/delete.png"
                }

                Rectangle {
                    id: trash_bin_back
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    color: "#000731"
                    radius: 25
                    z: -1
                    opacity: 0.5
                }

                RoundButton {
                    id: trash_but
                    width: 50
                    height: 50
                    text: ""
                    opacity: 0.5
                }


            }

            Rectangle {
                id: add
                x: 0
                width: 50
                height: 50
                color: "#00000000"

                Image {
                    id: add_icon
                    x: 0
                    y: 0
                    width: 40
                    height: 40
                    source: "res/add.png"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }

                Rectangle {
                    id: add_back
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    color: "#000731"
                    radius: 25
                    opacity: 0.5
                    z: -1
                }

                RoundButton {
                    id: add_but
                    width: 50
                    height: 50
                    text: ""
                    opacity: 0.5
                }
            }

            Rectangle {
                id: accept
                x: 0
                width: 50
                height: 50
                color: "#00000000"
                opacity: 1

                Image {
                    id: accept_icon
                    x: 0
                    y: 0
                    width: 40
                    height: 40
                    source: "res/accept.png"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }

                Rectangle {
                    id: accept_back
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    color: "#000731"
                    radius: 25
                    opacity: 0.5
                    z: -1
                }

                RoundButton {
                    id: accept_but
                    width: 50
                    height: 50
                    text: ""
                    visible: true
                    opacity: 0.5
                }
            }
        }

        ColumnLayout {
            id: advice_pack
            x: 40
            y: 20
            width: 720
            height: 528
            anchors.verticalCenterOffset: 30
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            visible: true
            Rectangle {
                id: advice1
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                Text {
                    id: scenario1
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("Visit Pusha-Voditsya (most popular)")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton7
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }

            Rectangle {
                id: advice2
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                Text {
                    id: scenario2
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("Go to Potapov Park (nearest location)")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox1
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike1
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but1
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like1
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton8
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }

            Rectangle {
                id: advice3
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                Text {
                    id: scenario3
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("Kayaking seems awesome")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox2
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike2
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but2
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like2
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton9
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }

            Rectangle {
                id: advice4
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                Text {
                    id: scenario4
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("Play badminton (sport-based)")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox3
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike3
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but3
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like3
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton10
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }

            Rectangle {
                id: advice5
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                Text {
                    id: scenario5
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("Visit Hydropark (user)")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox4
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike4
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but4
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like4
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton11
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }

            Rectangle {
                id: advice6
                y: 0
                width: 720
                height: 50
                color: "#00000000"
                opacity: 1
                Text {
                    id: scenario6
                    x: 81
                    y: 0
                    width: 512
                    height: 50
                    color: "#78bdd8"
                    text: qsTr("ПЩПЩПЩПЩП (user)")
                    lineHeight: 0.9
                    font.family: "MS Shell Dlg 2"
                    visible: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignLeft
                }

                CheckBox {
                    id: checkBox5
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    font.family: "Times New Roman"
                    focusPolicy: Qt.NoFocus
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    id: dislike5
                    x: 660
                    width: 40
                    height: 40
                    opacity: 1
                    source: "res/dislike.png"
                    RoundButton {
                        id: dislike_but5
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }

                Image {
                    id: like5
                    x: 609
                    y: 0
                    width: 40
                    height: 40
                    source: "res/like.png"
                    RoundButton {
                        id: roundButton12
                        width: 40
                        height: 40
                        text: ""
                        opacity: 0.3
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 0
                }
            }
            spacing: 10
        }

    }

    Connections {
        target: roundButton2
        onClicked: {
            var component = Qt.createComponent("MAin_window.qml")
            var window    = component.createObject(mainwindow)
            window.show
        }
    }

    Connections {
        target: add_but
        onClicked: {
            var component = Qt.createComponent("AddAdvice_window.qml")
            var window    = component.createObject(mainwindow)
            window.show
            }
    }



}








































































